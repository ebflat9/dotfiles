#!/usr/bin/env bash

# Ansi color codes
BOLD_GREEN="\e[1;32m"
BOLD_WHITE="\e[1;37m"
BOLD_RED="\e[1;31m"
RESET_TXT="\e[0m"

function usage() {
	printf "Usage: %s [-g]\n\n" "${0}"
	printf "\t g | git - initialize a git repo as well\n"
}

function error_exit() {
	printf "${BOLD_RED}%s${RESET_TXT}\n" "${*}"
	exit 1
}

printf "${BOLD_WHITE}Create git repo? (y|n): ${RESET_TXT}"
read -r REPLY

if [[ "${REPLY}" =~ (y|Y) ]]; then
	git init || error_exit "Unable to create git repo"
fi

printf "${BOLD_GREEN}%s${RESET_TXT}\n" "Creating .gitignore"
echo "node_modules" >.gitignore || error_exit "Unable to create .gitignore"

printf "${BOLD_GREEN}%s${RESET_TXT}\n" "Initializing package.json"
cat <<EOF >package.json
{
  "name": "$(basename "${PWD}")",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "type": "module",
  "scripts": {
	"start": "node index.js",
    "test": "mocha --parallel",
	"dev": "nodemon index.js"
  },
  "keywords": [],
  "author": "Erin Burton",
  "license": "MIT",
  "devDependencies": {}
}
EOF

printf "${BOLD_GREEN}%s${RESET_TXT}\n" "Installing packages"
npm install @ebflat9/fp
npm install --save-dev \
	eslint \
	mocha \  || error_exit "Unable to install packages"

touch index.js

printf "${BOLD_GREEN}%s${RESET_TXT}\n" "Creating base .eslintrc.json"
cat <<EOF >.eslintrc.json || error_exit "Unable to create .eslintrc.json"
{
	"root": true,
    "env": {
        "es2021": true,
        "shared-node-browser": true,
        "node": true,
        "browser": true,
        "mocha": true
    },
    "extends": [
		"eslint:recommended",
    ],
	"parserOptions": {
		"ecmaVersion": "latest"
	},
    "rules": {
        "no-promise-executor-return": "warn",
        "require-atomic-updates": "error",
        "accessor-pairs": "warn",
        "array-callback-return": "error",
        "complexity": "error",
        "consistent-return": "warn",
        "curly": [
            "error",
            "multi-line"
        ],
        "dot-notation": "warn",
        "eqeqeq": [
            "error",
            "smart"
        ],
        "grouped-accessor-pairs": [
            "error",
            "getBeforeSet"
        ],
        "no-magic-numbers": [
            "warn",
            {
                "ignoreArrayIndexes": true,
                "enforceConst": true,
                "ignore": [
                    1,
                    -1,
                    0
                ]
            }
        ],
        "no-unused-vars": "warn",
        "no-param-reassign": "warn",
        "prefer-promise-reject-errors": "error",
        "no-var": "error",
        "prefer-const": [
            "warn",
            {
                "destructuring": "any"
            }
        ],
        "no-unneeded-ternary": "error",
        "no-duplicate-imports": [
            "error",
            {
                "includeExports": true
            }
        ],
        "no-constant-condition": [
            "error",
            {
                "checkLoops": false
            }
        ],
        "func-names": [
            "warn",
            "always"
        ],
        "object-shorthand": "warn"
    }
}
EOF

printf "${BOLD_WHITE}%s${RESET_TXT}\n" "Initialization completed"
