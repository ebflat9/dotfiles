#!/bin/sh
sed -i \
         -e 's/#0f191f/rgb(0%,0%,0%)/g' \
         -e 's/#c7d6d0/rgb(100%,100%,100%)/g' \
    -e 's/#0f191f/rgb(50%,0%,0%)/g' \
     -e 's/#66A5AD/rgb(0%,50%,0%)/g' \
     -e 's/#0f191f/rgb(50%,0%,50%)/g' \
     -e 's/#c7d6d0/rgb(0%,0%,50%)/g' \
	"$@"
